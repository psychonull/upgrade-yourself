pacjam
===

### Development
```bash
npm install
```

### Grunt tasks

Compile:
```bash
grunt
grunt test # this does not test
grunt dist
```

![C DOS RUN RUN DOS RUN](http://i.imgur.com/RaEsv4E.jpg)


Fonts used: 
http://ptless.org/sfomi.html
http://www.dafont.com/kolquatfunkybitm.font
