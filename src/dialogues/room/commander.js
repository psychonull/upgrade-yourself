module.exports = [
  {
    owner: 'c',
    value: 'Thanks for dialing in.'
  },
  {
    owner: 'c',
    value: 'I bet you don\'t know how special you are.'
  },
  {
    owner: 'c',
    value: 'It\'s the first time I have an eighty-twenty assigned'
  },
  {
    options: [
      {
        code: 'A',
        value: 'eighty-twenty? what\'s that?',
        dialogue: [{
            owner: 'p',
            value: 'eighty-twenty? what\'s that?'
          },
          {
            owner: 'c',
            random: ['Means that you are twenty percent human.. eighty electronic.']
          },
          {
            owner: 'c',
            value: 'You are first of your kind to fill a job in a federal agency.'
          },
          {
            owner: 'c',
            value: 'Until now.. The STATE has only employed humans and those ETs from Asteroid A22.'
          },
          {
            owner: 'c',
            value: 'So much talk. Grab this. Fill your job.'
          },
          {
            owner: 'c',
            value: 'and do it well. for the sake of posterity.'
          }
        ]
      },
      {
        code: 'B',
        value: 'I feel sexy wearing this cyborg suit.',
        dialogue: [
          {
            owner: 'p',
            value: 'I feel sexy wearing this cyborg suit.'
          },
          {
            owner: 'c',
            random: ['Because you are. Not only that...']
          },
          {
            owner: 'c',
            value: 'You are first of your kind to fill a job in a federal agency.'
          },
          {
            owner: 'c',
            value: 'Until now.. The STATE has only employed humans and those ETs from Asteroid A22.'
          },
          {
            owner: 'c',
            value: 'So much talk. Grab this. Fill your job.'
          },
          {
            owner: 'c',
            value: 'and do it well. for the sake of posterity... Sexy cyborg.'
          }
        ]
      }
    ]
  }
];
