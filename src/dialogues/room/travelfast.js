module.exports = [{
  owner: 'player',
  value: '... Hey... Hello??'
},
{
  owner: 't',
  value: 'Sorry, but I was programmed to only ' +
  'say that I was not programmed to talk'
},
{
  options: [
  {
    code: 'A',
    value: 'Thats weird.',
    dialogue: [{
      owner: 't',
      value: 'Sorry, but I was programmed to only ' +
      'say that I was not programmed to talk'
    },
    {
      owner: 'player',
      random: ['Got it.', 'Ok, I will just walk away']
    }]
  },
  {
    code: 'B',
    value: 'Companies have not changed anything. I\'m sure there is a premium version of you that\'s just the same with the talk flag enabled.',
    dialogue: [
    {
      owner: 't',
      value: 'Sorry, but I was programmed to only ' +
      'say that I was not programmed to talk'
    },
    {
      owner: 'player',
      random: ['Got it.', 'Ok, I will just walk away']
    }]
  },
  {
    code: 'C',
    value: 'Ok, I will just walk away'
  }
  ]
}];
