var pac = require('../../../pac/src/');

var WalkableScene = pac.Scene.extend({

  init: function(){

    this.walkableArea = new pac.prefabs.WalkableArea({
      layer: 'background',
      shape: this.walkableShape,
      commands: [ 'walkto' ]
    });

    var blackList = [ 'use','give','talkto','push','open',
      'close','pickup','lookat' ];

    blackList.forEach(function(cmd){
      this.walkableArea.onCommand[cmd] = false;
    }, this);

    this.walkableArea.hiddenCommands = true;
  },

  onEnter: function(){
    this.addObject(this.walkableArea);
  },

  onExit: function(){
    this.walkableArea.clearWalkers();
  },

});

module.exports = WalkableScene;
