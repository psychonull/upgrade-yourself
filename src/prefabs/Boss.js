var pac = require('../../../pac/src/');

var bossAnimations = new pac.AnimationList({
  idle: new pac.Animation({
    fps: 6,
    frames: ['idle_0','idle_1','idle_2','idle_3', 'idle_4', 'idle_5']
  })
}, {
  default: 'idle',
  autoplay: true
});

var Boss = pac.Sprite.extend({
  name: 'The Boss',
  texture: 'boss',
  layer: 'background',
  shape: true,
  actions: [
  new pac.actions.Commander(),
  new pac.actions.Speaker({
    textOptions: {
      wordWrap: 120,
      font: '6px lucas',
      isBitmapText: true,
      tint: 0x555555
    },
    offset: new pac.Point(-20,-10),
    smartPosition: false
  })
  ],
  animations: bossAnimations,
  size: {
    width: 48,
    height: 36
  },

  init: function(){
  },

  update: function(dt){
  },

  onEnterScene: function(){}

});

module.exports = Boss;
