var pac = require('../../../pac/src/');
var Speaker = require('../actions/Speaker.js');
var howler = require('howler');

var playerAnimations = new pac.AnimationList({
  walkRight: new pac.Animation({
    fps: 6,
    frames: ['right_0','right_1','right_2','right_3']
  }),
  walkLeft: new pac.Animation({
    fps: 6,
    frames: ['left_0','left_1','left_2','left_3']
  }),
  idleRight: new pac.Animation({
    fps: 8,
    frames: ['idle_0','idle_1','idle_2','idle_3','idle_4',
      'idle_0','idle_1','idle_2','idle_3','idle_4','idle_eyes_closed']
  }),
  idleLeft: new pac.Animation({
    fps: 8,
    frames: ['idle_0','idle_1','idle_2','idle_3','idle_4',
      'idle_0','idle_1','idle_2','idle_3','idle_4','idle_eyes_closed']
  })
}, {
  default: 'idleRight',
  autoplay: true
});

var walkToFX;
var playingFX = false;

var Player = pac.Sprite.extend({
  name: 'Myself',
  texture: 'player',
  layer: 'front',
  zIndex: 1000,
  shape: true,
  actions: [
    new Speaker({ tint: 0xFFFFFF }),
    new pac.actions.AutoZIndex(true, 'feet')
  ],
  animations: playerAnimations,
  size: {
    width: 32,
    height: 70
  },

  sPlayWalkTo: function() {
      if (!walkToFX){
        walkToFX = new howler.Howl({
          urls: ['assets/sounds/walkTo.mp3'],
          autoplay: false,
          loop: true,
          volume: 0.7
        });
      }
      if (walkToFX){
        if (playingFX === true) {
          return;
        }
        if (playingFX === false){
          walkToFX.volume(0.7);
          walkToFX.play();
          playingFX = true;
        }
      }
  },

  onFadeComplete: function() {
    walkToFX.stop();
    playingFX = false;
  },

  sStopWalkTo: function() {
      if (walkToFX && playingFX === true) {
        walkToFX.fadeOut(0, 100, this.onFadeComplete);
      }
  },

  init: function(){
    this.lastSide = 'Left';

    this.walkerAct = {
      velocity: 40,
      feet: new pac.Point(17, 68)
    };

  },

  update: function(dt){

    if (this.frozen){
      return;
    }
    if (this.walkingTo){
      var dir = this.walkingTo;
      if (dir.x >= 0){
        this.animations.play('walkRight');
        this.lastSide = 'Right';
      }
      else {
        this.animations.play('walkLeft');
        this.lastSide = 'Left';
      }
      this.sPlayWalkTo();
    }
    else {
      this.animations.play('idle' + this.lastSide);
      this.sStopWalkTo();
    }
  },

  onEnterScene: function(){

    this.actions
    .removeAll(pac.actions.WalkTo)
    .removeAll(pac.actions.Walker)
    .add(new pac.actions.Walker(this.walkerAct));

  }

});

module.exports = Player;
