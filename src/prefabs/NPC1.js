var pac = require('../../../pac/src/');

var npcAnimations = new pac.AnimationList({
  idle: new pac.Animation({
    fps: 1,
    frames: ['idle','idle','idle','idle_eyes_closed']
  })
}, {
  default: 'idle',
  autoplay: true
});

var NPC1 = pac.Sprite.extend({
  name: 'My first colleague',
  texture: 'npc1',
  layer: 'background',
  shape: true,
  actions: [
  new pac.actions.Commander()
  ],
  animations: npcAnimations,
  size: {
    width: 32,
    height: 70
  },

  init: function(){
  },

  update: function(dt){
    var keys = this.game.inputs.get('keyboard').keys;
    if(keys.A.isDown){
      this.position.x--;
    }
    if(keys.D.isDown){
      this.position.x++;
    }
    if(keys.W.isDown){
      this.position.y--;
    }
    if(keys.S.isDown){
      this.position.y++;
    }
  },

  onEnterScene: function(){}

});

module.exports = NPC1;
