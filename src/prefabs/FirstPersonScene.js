var pac = require('../../../pac/src/');

var FirstPersonScene = pac.Scene.extend({

  hideCommandBar: true,

  init: function(options){
    this.player = null;
    this.commandBar = null;
    this.inventory = null;
    if(options && options.hideCommandBar === false){
      this.hideCommandBar = false;
    }
  },

  onEnter: function(){
    this.player = this.game.findOne('Myself');
    this.player.visible = false;
    this.player.active = false;
    if(this.hideCommandBar){
      this.commandBar = this.game.findOne('CommandBar');
      this.inventory = this.game.findOne('Inventory');
      this.commandBar.visible = false;
      this.commandBar.active = false;
      this.inventory.visible = false;
      this.inventory.active = false;
    }
  },

  onExit: function(){
    this.player.visible = true;
    this.player.active = true;
    this.player = null;
    if(this.hideCommandBar){
      this.commandBar.visible = true;
      this.commandBar.active = true;
      this.commandBar = null;
      this.inventory.visible = true;
      this.inventory.active = true;
      this.inventory = null;
    }
  },

});

module.exports = FirstPersonScene;
