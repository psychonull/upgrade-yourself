
var inventory = new pac.prefabs.Inventory({

  position: new pac.Point(160, 150),
  size: { width: 200, height: 50 },

  maxItems: 12,

  style: {

    itemsPerRow: 6,

    position: new pac.Point(30, 10),
    margin: { x: 0, y: 0 },
    size: { width: 18, height: 18 },

    holder: {
      stroke: '#ffffff',
      lineWidth: 0.5,
      fill: '#000000',
    }

  },

});

module.exports = inventory;