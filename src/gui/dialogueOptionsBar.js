var pac = require('../../../pac/src/');
/*
var toggleElement = function(element, bool){
  element.visible = bool;
  element.active = bool;
};
*/

var CustomDialogueOptionsBar = pac.prefabs.DialogueOptionsBar.extend({

  showOptions: function(options, cb){
    this.game.cinematic(true);
    this.constructor.__super__.showOptions.call(this, options, cb);
  },

  onOptionSelected: function(code){
    /*var commandBar = this.game.findOne('CommandBar');
    var inventory = this.game.findOne('Inventory');
    toggleElement(commandBar, true);
    toggleElement(inventory, true);*/
    this.constructor.__super__.onOptionSelected.call(this, code);
  }

});

var optionsBar = new CustomDialogueOptionsBar({
  layer: 'gui',

  position: new pac.Point(0, 150),
  size: { width: 320, height: 50 },

  fill: '#000000',

  style: {

    margin: { x: 5, y: 3 },
    size: { width: 310, height: 13 },

    text: {
      font: '4px lucaswhite',
      isBitmapText: true,
      tint: 0xAAAAAA,
      wordWrap: 300
    },

    hover:{
      font: '4px lucaswhite',
      isBitmapText: true,
      tint: 0xFFFFFF,
      wordWrap: 300
    },

    active: {
      font: '4px lucaswhite',
      isBitmapText: true,
      tint: 0xAAAAAA,
      wordWrap: 300
    }
  }

});

module.exports = optionsBar;
