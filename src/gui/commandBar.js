var pac = require('../../../pac/src/');
var Speak = require('../actions/Speak.js');

var jamCommandBar = pac.prefabs.CommandBar.extend({

  showCannotMessage: function(obj, message){
    if (this._isCommandDisabled(obj) || this._isCommandHidden(obj)){
      return;
    }

    this.lastRequestOf = (obj && obj.cid) || null;

    if (!message){
      message = this.cannotHolder.replace(/{{action}}/ig, this._current.value);
    }

    this.messageBox.value = '';

    this.game.findOne('Myself')
      .actions.pushBack(new Speak({text: message}));
  }

});


var commBar = new jamCommandBar({
  layer: 'gui',

  position: new pac.Point(0, 150),
  size: { width: 320, height: 50 },

  fill: '#000000',
  stroke: '#000000',

  cannotHolder: 'I certain cannot {{action}} that',

  messageBox: {
    position: new pac.Point(15, 2),
    font: '6px lucaswhite',
    isBitmapText: true
  },

  commands: {
    'use': 'Use',
    'give': 'Give',

    'talkto': 'Talk to',
    'push': 'Push',

    'open': 'Open',
    'close': 'Close',

    'pickup': 'Pick Up',
    'lookat': 'Look At',
    'walkto': 'Walk To',
  },

  inventoryCommands: {
    'use': 'with',
    'give': 'to',
    'open': 'with'
  },

  current: 'walkto',

  style: {

    position: new pac.Point(10, 8),
    margin: { x: 5, y: 3 },
    size: { width: 40, height: 10 },

    text: {
      font: '12px funkygray',
      isBitmapText: true,
      fill: '#ffffff'
    },

    hover:{
      font: '12px funkywhite',
      isBitmapText: true,
      fill: '#ffff00'
    },

    active: {
      font: '12px funkywhite',
      isBitmapText: true,
      fill: '#00ff00'
    },

    grid: [
      ['push',   'open',  'walkto'],
      ['talkto', 'close', 'pickup'],
      ['use',    'give',  'lookat']
    ],
  }

});

module.exports = commBar;
