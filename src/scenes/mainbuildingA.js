var pac = require('../../../pac/src/');
var _ = pac._;
var WalkableScene = require('../prefabs/WalkableScene.js');

var MainBuildingAScene = WalkableScene.extend({

  size: { width: 320, height: 200 },
  texture: 'mainbuilding_A_bg',
  walkableShape: new pac.Polygon(
    [11,119,319,119,319,149,0,149]
  ),

  onEnter: function(scene){
    MainBuildingAScene.__super__.onEnter.apply(this, arguments);

    if (scene){
      switch(scene.name){
        case 'mainbuildingB':
          this.game.player.position = new pac.Point(270, 70);
          break;
        case 'servers':
          this.game.player.position = new pac.Point(50, 70);
          break;
      }
    }
    else {
      this.game.player.position = new pac.Point(270, 70);
    }

    var inventory = this.game.findOne('Inventory');

    var MainBuildingSprite = pac.Sprite.extend({
      texture: 'mainbuilding_props',
      layer: 'background',
      shape: true,
      position: {
        x: 280,
        y: 20
      },
      size: {
        width: 32,
        height: 32
      }
    });

    var ductClosed = new MainBuildingSprite({
      frame: 'duct_closed',
      name: 'Ventilation duct',
      position: {
        x:18,
        y:75
      },
      size: {
        width: 52,
        height: 52
      },
      actions: [ new pac.actions.Commander() ]
    });

    var serverDisk = new pac.Sprite({
      texture: 'updater',
      layer: 'background',
      zIndex: 50,
      name: 'server disk',
      shape: true,
      position: {
        x: 40,
        y: 100
      },
      size: {
        width: 12,
        height: 12
      },
      visible: false,
      active: false,
      actions: [ new pac.actions.Commander() ]
    });

    serverDisk.active = false;
    serverDisk.visible = false;

    serverDisk.onCommand = {
      lookat:function(){
        return 'That looks like a disk, I don\'t know whats for';
      },
      pickup: function(){

        this.visible = false;
        this.active = false;

        var serverDiskInv = new pac.Sprite({
          texture: 'updater',
          name: 'server disk',
          actions: [ new pac.actions.Commander() ]
        });
/*
        serverDiskInv.onCommand = {
          use: function(withName){

            if (!withName){
              return 'I should use this with something';
            }

            switch (withName){
              case 'Active Rack':
                inventory.remove(this.name);
                return false;
            }

            return 'I don\'t know how to use it with ' + withName;
          }
        };
*/
        inventory.add(serverDiskInv);
        return false;
      }
    };

    ductClosed.onCommand = {
      open: function(){
        this.position = new pac.Point(2, 75);
        this.frame = 'duct_open';

        serverDisk.visible = true;
        serverDisk.active = true;
      },
      close: function(){
        this.position = new pac.Point(18, 75);
        this.frame = 'duct_closed';

        serverDisk.visible = false;
        serverDisk.active = false;
      }
    };

    var changeScene = new pac.GameObject({
      position: new pac.Point(280, 110),
      shape: new pac.Rectangle({ size: { width: 50, height: 40 }}),

      actions: [ new pac.actions.Commander() ]
    });

    changeScene.hiddenCommands = true;
    changeScene.onCommand = {
      walkto: function(){
        this.game.loadScene('mainbuildingB');
      }
    };

    var computerDoor = new MainBuildingSprite({
      frame: 'computer_door',
      name: 'Door to Servers',
      position: {
        x:0,
        y:0
      },
      size: {
        width: 11,
        height: 150
      },
      actions: [ new pac.actions.Commander() ]
    });

    computerDoor.onCommand = {
      open: function(){
        //this.frame = 'computer_door_open';
        this.game.loadScene('servers');
      }
    };

    this
      .addObject(serverDisk)
      .addObject(computerDoor)
      .addObject(changeScene)
      .addObject(ductClosed);

  },

  onExit: function(scene){
    MainBuildingAScene.__super__.onExit.apply(this, arguments);
  },

  update: function(dt){

  }
});

module.exports = MainBuildingAScene;
