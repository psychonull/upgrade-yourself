var MenuScene = require('./menu.js'),
  RoomScene = require('./room.js'),
  TravelfastScene = require('./travelfast.js'),
  MainBuildingBScene = require('./mainbuildingB.js'),
  MainBuildingAScene = require('./mainbuildingA.js'),
  Room2Scene = require('./room2.js'),
  EndScene = require('./end.js'),
  ServersScene = require('./servers.js');

module.exports = {
  'menu': new MenuScene(),
  'room': new RoomScene(),
  'travelfast': new TravelfastScene(),
  'mainbuildingB': new MainBuildingBScene(),
  'mainbuildingA': new MainBuildingAScene(),
  'servers': new ServersScene(),
  'room2': new Room2Scene(),
  'end': new EndScene()
};
