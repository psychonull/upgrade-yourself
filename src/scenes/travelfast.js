var pac = require('../../../pac/src/');
var FirstPersonScene = require('../prefabs/FirstPersonScene.js');
var _ = pac._;

var TravelfastScene = FirstPersonScene.extend({

  size: { width: 320, height: 200 },
  texture: 'travelfast_bg',

  onEnter: function(scene){
    TravelfastScene.__super__.onEnter.apply(this, arguments);

    var currentSceneName = (scene && scene.name) || 'room';

    var Travelfast = pac.Sprite.extend({
      texture: 'travelfast_machine_full',
      layer: 'background',
      shape: false,
      position: {
        x: 0,
        y: 0
      },
      size: {
        width: 320,
        height: 200
      }
    });

    var travelfast = new Travelfast({});

    var noPointsUnlocked = new pac.Text({
      value: 'No locations unlocked.',
      font: '8px lucaswhite',
      isBitmapText: true,
      position: {
        x:95,
        y:17
      },
      wordWrap: 225 - 95
    });

    var exitBtn = new pac.Text({
      value: 'Exit',
      font: '10px lucaswhite',
      isBitmapText: true,
      position: {
        x:194,
        y:130
      },
      tint: 0xAAAAAA,
      wordWrap: 225 - 95,
      actions: [new pac.actions.Hoverable(), new pac.actions.Clickable()],
      shape: new pac.Rectangle({ size: {
        width:50,
        height: 15
      } })
    });

    exitBtn
      .on('hover:in', function(){
        this.tint = 0xFFFFFF;
      })
      .on('hover:out', function(){
        this.tint = 0xAAAAAA;
      })
      .on('click', function(){
        this.game.loadScene(currentSceneName);
      });

    var mainBuildingBtn = new pac.Text({
      value: 'Federal Historic Preservation Institute - Main Building',
      font: '6px lucaswhite',
      isBitmapText: true,
      position: {
        x:95,
        y:17
      },
      shape: new pac.Rectangle({ size: {
        width:200,
        height: 15
      } }),
      wordWrap: 225 - 95,
      tint: 0x005500,
      actions: [new pac.actions.Hoverable(), new pac.actions.Clickable()]
    });

    mainBuildingBtn
      .on('hover:in', function(){
        this.tint = 0xFFFFFF;
      })
      .on('hover:out', function(){
        this.tint = 0x005500;
      })
      .on('click', function(){
        this.game.loadScene('mainbuildingB');
      });

    mainBuildingBtn.visible =
      this.game.state.locations.indexOf('mainbuildingB') !== -1;
    mainBuildingBtn.active =
      this.game.state.locations.indexOf('mainbuildingB') !== -1;

    var room2Btn = new pac.Text({
      value: 'HOME',
      font: '6px lucaswhite',
      isBitmapText: true,
      position: {
        x:95,
        y:17
      },
      shape: new pac.Rectangle({ size: {
        width:200,
        height: 15
      } }),
      wordWrap: 225 - 95,
      tint: 0x005500,
      actions: [new pac.actions.Hoverable(), new pac.actions.Clickable()]
    });

    room2Btn
      .on('hover:in', function(){
        this.tint = 0xFFFFFF;
      })
      .on('hover:out', function(){
        this.tint = 0x005500;
      })
      .on('click', function(){
        this.game.loadScene('room2');
      });

    room2Btn.visible =
      this.game.state.locations.indexOf('room2') !== -1;
    room2Btn.active =
      this.game.state.locations.indexOf('room2') !== -1;

    noPointsUnlocked.visible = this.game.state.locations.length === 0;

    this
      .addObject(travelfast)
      .addObject(noPointsUnlocked)
      .addObject(mainBuildingBtn)
      .addObject(room2Btn)
      .addObject(exitBtn);

  },

  onExit: function(scene){
    TravelfastScene.__super__.onExit.apply(this, arguments);
  },

  update: function(dt){

  }
});

module.exports = TravelfastScene;
