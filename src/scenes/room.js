var pac = require('../../../pac/src/');
var _ = pac._;
var WalkableScene = require('../prefabs/WalkableScene.js');
var Speaker = require('../actions/Speaker.js');
var Dialoguer = require('../actions/Dialoguer.js');
var howler = require('howler');

var bgMusic;

var RoomScene = WalkableScene.extend({

  size: { width: 320, height: 200 },
  texture: 'room_bg',
  walkableShape: new pac.Polygon(
    [ 0,140 , 25,127 , 307,126 , 320,132, 320,150, 0,150 ]
  ),

  onEnter: function(scene){
    RoomScene.__super__.onEnter.apply(this, arguments);

    bgMusic = new howler.Howl({
      urls: ['assets/sounds/room01.mp3'],
          autoplay: true,
          loop: true,
          volume: 0.7
    });

    if(!this.game.state.room){
      this.game.state.room = {
        firstTime: true,
        locationDecoderThrown: false,
        locationDecoderPickup: false
      };
    }

    var inventory = this.game.findOne('Inventory');

    var RoomSprite = pac.Sprite.extend({
      texture: 'room_props',
      layer: 'background',
      shape: true,
      position: {
        x: 280,
        y: 20
      },
      size: {
        width: 32,
        height: 32
      }
    });

    var tube = new RoomSprite({
      frame: 'tube',
      name: 'Object transport device',
      layer: 'front',
      zIndex: 140,
      position: {
        x:11,
        y:0
      },
      size: {
        width: 26,
        height: 116
      },
      actions: [ new pac.actions.Commander() ]
    });

    tube.onCommand = {
      use: function(){
        return _.sample([
          'I cannot make anything with it',
          'I would need centuries to think about a good use for that.',
        ]);
      },
      pickup: function(){
        return _.sample([
          'I guess I could pick it up if I were a giant.'
        ]);
      },
      lookat: function(){
        return _.sample([
          'Large, flexible, old tube. It\'s like a reverse chimney.',
        ]);
      }
    };

    var travelfast = new RoomSprite({
      frame: 'travelfast',
      name: 'Travelfast Machine Lite Edition',
      position: {
        x:163,
        y:65
      },
      size: {
        width:43,
        height:69
      },
      actions: [
        new pac.actions.Commander(),
        new Speaker({ tint: 0xFFAAFF })
      ]
    });

    travelfast.onCommand = {
      lookat: function(){
        return _.sample([
          'Looks like a cashier from the nineties..',
          'it reads "Travelfast Lite Edition. Buy the PRO edition this christmas."',
          'HAHA sure. This is supposed to teleport stuff.'
        ]);
      },
      use: function(withName){
        if(!withName){
          this.game.loadScene('travelfast');
          return;
        }
        switch (withName){
          case 'location decoder':
            inventory.remove('location decoder');
            this.game.state.locations.push('mainbuildingB');
            return false;
        }

        return 'I don\'t know how to use it with ' + withName;
      }
    };

    travelfast.actions.pushFront(new Dialoguer({
      characters: {
        t: travelfast,
        player: this.game.findOne('Myself')
      },
      dialogue: require('../dialogues/room/travelfast.js'),
      command: 'talkto'
    }));

    var charger = new RoomSprite({
      frame: 'charger',
      name: 'Charge station',
      layer: 'front',
      position: {
        x:264,
        y:121
      },
      size: {
        width:33,
        height:19
      },
      actions: [ new pac.actions.Commander(), new pac.actions.AutoZIndex() ]
    });

    charger.onCommand = {
        lookat: function(){
          return _.sample([
            'The label reads: "Rest your body. Recharge your soul."'
            ]);
        },
        use: function(){
          return 'I don\'t need this!';
        }
      };

    var chargerCables = new RoomSprite({
      frame: 'charger_cables',
      name: 'Charge station cables',
      position: {
        x:295,
        y:128
      },
      size: {
        width:25,
        height:22
      },
      actions: [ new pac.actions.Commander() ]
    });

    chargerCables.onCommand = {
      lookat: function(){
        return 'Where those come frome?? Thickest industrial cables I\'ve ever seen.';
      },
      pickup: function(){
        return 'Where those come frome?? Thickest industrial cables I\'ve ever seen.';
      }
    };

    var ancientFlag = new RoomSprite({
      frame: 'flag',
      name: 'Ancient flag replica',
      position: {
        x:261,
        y:21
      },
      size: {
        width:21,
        height:21
      },
      actions: [ new pac.actions.Commander() ]
    });

    ancientFlag.onCommand = {
      lookat: function(){
        return _.sample([
          'It\'s the flag I used to wear in those times...'
          ]);
        },
        pickup: function(){
          return 'It\'s so high.';
        }
      };


    var viewpointAnimations = new pac.AnimationList({
      clouds: new pac.Animation({
        fps: 1,
        frames: ['viewpoint','viewpoint_1','viewpoint_2','viewpoint_3']
      }),
      commander: new pac.Animation({
        fps:2,
        frames: [
          'viewpoint_comm_0', 'viewpoint_comm_1', 'viewpoint_comm_2',
          'viewpoint_comm_2', 'viewpoint_comm_2', 'viewpoint_comm_1',
          'viewpoint_comm_0', 'viewpoint_comm_0', 'viewpoint_comm_2',
          'viewpoint_comm_1', 'viewpoint_comm_2', 'viewpoint_comm_0'
        ]
      })
    }, {
      default: 'clouds',
      autoplay: true
    });

    var viewpoint = new RoomSprite({
      frame: 'viewpoint',
      name: 'Outside view',
      shape: new pac.Rectangle({
        size: { width: 63, height: 55 }
      }),
      animations: viewpointAnimations,
      position: {
        x:39,
        y:39
      },
      size: {
        width:63,
        height:60
      },
      actions: [
        new pac.actions.Commander() ,
        new Speaker({ tint: 0xFF5555 })
      ]
    });

    viewpoint.actions.pushFront(
      new Dialoguer({
        characters: {
          p: this.game.player,
          c: viewpoint
        },
        dialogue: require('../dialogues/room/commander.js'),
        command: 'commanderunblock'
      })
    );

    viewpoint.onCommand = {
      lookat: function(){
        return _.sample([
          'Looks like the last sunday.',
          'I am a countryside person. But that reminds me about windows..'
        ]);
      }
    };

    var viewpointController = new RoomSprite({
      frame: 'viewpoint_controller_off',
      name: 'Outside view control panel',
      shape: new pac.Rectangle({
        position: new pac.Point(-5, 0),
        size: { width: 40, height: 6 }
      }),
      position: {
        x:60,
        y:95
      },
      size: {
        width:30,
        height:4
      },
      actions: [ new pac.actions.Commander() ]
    });

    viewpointController.onCommand = {
      push: function(){
        if(this.game.state.room.locationDecoderThrown){
          return 'This doesn\'t seem to work anymore';
        }
        this.frame = 'viewpoint_controller_on';
        viewpoint.animations.play('commander');
        viewpoint.onCommand.commanderunblock();
        viewpoint.dialogue.on('end', _.bind(function(){
          locationDecoder.visible = true;
          locationDecoder.active = true;
          this.game.state.room.locationDecoderThrown = true;
          viewpoint.animations.play('clouds');
        }, this));
      },
      use: function(){
        if(this.game.state.room.locationDecoderThrown){
          return 'This doesn\'t seem to work anymore';
        }
        this.frame = 'viewpoint_controller_on';
        viewpoint.animations.play('commander');
        viewpoint.onCommand.commanderunblock();
        viewpoint.dialogue.on('end', _.bind(function(){
          locationDecoder.visible = true;
          locationDecoder.active = true;
          this.game.state.room.locationDecoderThrown = true;
          viewpoint.animations.play('clouds');
        }, this));
      },
      lookat: function(){
        return 'It\'s like a button. Like a button I\'ve never seen';
      }
    };

    var locationDecoder = new pac.Sprite({
      texture: 'location_decoder',
      layer: 'background',
      name: 'location decoder',
      shape: true,
      position: {
        x: 20,
        y: 128
      },
      size: {
        width: 12,
        height: 12
      },
      visible: false,
      actions: [ new pac.actions.Commander() ]
    });
    locationDecoder.active = false;

    if(this.game.state.room.locationDecoderThrown &&
        !this.game.state.room.locationDecoderPickup){
          locationDecoder.visible = true;
          locationDecoder.active = true;
        }

    locationDecoder.onCommand = {
      pickup: function(){
        this.visible = false;
        this.active = false;
        this.game.state.room.locationDecoderPickup = true;

        var locationDecoderInv = new pac.Sprite({
          texture: 'location_decoder',
          name: 'location decoder',
          actions: [ new pac.actions.Commander() ]
        });

        locationDecoderInv.onCommand = {
          use: function(withName){

            if (!withName){
              return 'I should use this with something';
            }

            switch (withName){
              case 'Travelfast Machine Lite Edition':
                inventory.remove(this.name);
                return false;
              }

              return 'I don\'t know how to use it with ' + withName;
            }
          };

          inventory.add(locationDecoderInv);
          return false;
      },
      lookat: function(){
        return 'Location decoder? Do they encrypt locations now? I would love to bruteforce those locations.';
      }
    };

    this.addObject(tube)
      .addObject(travelfast)
      .addObject(charger)
      .addObject(ancientFlag)
      .addObject(viewpoint)
      .addObject(viewpointController)
      .addObject(locationDecoder)
      .addObject(chargerCables);


    if(this.game.state.room.firstTime){
      this.game.player.position = new pac.Point(265, 70);
      this.game.player.frozen = true;
      this.game.player.animations.stop();
      this.game.player.actions.pushFront(
        new Dialoguer({
          characters: {
            player: this.game.player
          },
          dialogue: require('../dialogues/room/first.js'),
          command: 'introduction'
        })
      );

      this.game.player.frozen = true;

      window.setTimeout(
        _.bind(function(){
          this.game.player.onCommand.introduction();
          this.game.player.dialogue.on('end', _.bind(function(){
            this.game.player.frozen = false;
            this.game.state.room.firstTime = false;
          },this));
        }, this), 20);
    }
    else {
      this.game.player.position = new pac.Point(172, 80);
    }
  },

  onExit: function(scene){
    RoomScene.__super__.onExit.apply(this, arguments);

    bgMusic.stop();
    bgMusic.unload();

  },

  update: function(dt){

  }
});

module.exports = RoomScene;
