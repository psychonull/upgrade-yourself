var pac = require('../../../pac/src/');
var _ = pac._;
var WalkableScene = require('../prefabs/WalkableScene.js');
var Boss = require('../prefabs/Boss.js');

var ServersScene = WalkableScene.extend({

  size: { width: 320, height: 200 },
  texture: 'servers_bg',
  walkableShape: new pac.Polygon(
    [11,119,319,119,319,149,0,149]
  ),

  onEnter: function(scene){
    ServersScene.__super__.onEnter.apply(this, arguments);

    this.game.player.position = new pac.Point(284, 74);

    var inventory = this.game.findOne('Inventory');

    var ServersSprite = pac.Sprite.extend({
      texture: 'servers_props',
      layer: 'background',
      shape: true,
      position: {
        x: 280,
        y: 20
      },
      size: {
        width: 32,
        height: 32
      }
    });

    var door = new ServersSprite({
      //frame: door_open
      frame: 'door_closed',
      name: 'Door to main building',
      position: {
        x:309,
        y:5
      },
      size: {
        width: 11,
        height: 150
      },
      actions: [ new pac.actions.Commander() ]
    });

    door.onCommand = {
      open: function(){
        this.game.loadScene('mainbuildingA');
      }
    };

    var serversAnimations = new pac.AnimationList({
      blink: new pac.Animation({
        fps: 12,
        frames: ['servers_left','servers_left_on']
      })
    }, {
      default: 'blink',
      autoplay: true
    });

    var servers = new ServersSprite({
      frame: 'servers_left',
      name: 'servers',
      position: {
        x:255,
        y:10
      },
      size: {
        width: 49,
        height: 117
      },
      animations: serversAnimations
    });

    var rackOn = new ServersSprite({
      frame: 'rack_on', //rack_on rack_off
      name: 'Active Rack',
      position: {
        x:257,
        y:80
      },
      size: {
        width: 20,
        height: 8
      },
      actions: [ new pac.actions.Commander() ]
    });

    rackOn.onCommand = {
      lookat: function(){
        return 'It looks like an empty rack';
      },
      use: function(obj){
        if (!obj){
          return 'I think I should put something in there';
        }

        if (obj === 'server disk'){
          screen.animations.play('matrix');
          inventory.remove(obj);
          this.game.state.locations.push('room2');
          //this.animations = serversAnimations;
          //this.animations.play('blink');
        }
      }
    };

    var rackError = new ServersSprite({
      frame: 'rack_error', //rack_on rack_off
      name: 'Rack',
      position: {
        x:257,
        y:101
      },
      size: {
        width: 20,
        height: 8
      },
      actions: [ new pac.actions.Commander() ]
    });

    rackError.onCommand = {
      lookat: function(){
        return 'It looks like a rack with an error';
      },
      use: function(obj){
        if (!obj){
          return 'I think I should put something in there';
        }

        if (obj === 'server disk'){
          return 'I cannot put a disk in it. It has an error.';
        }
      }
    };

    var screen = new ServersSprite({
      frame: 'screen_center_off',
      name: 'main screen',
      position: {
        x:83,
        y:40
      },
      size: {
        width: 56,
        height: 39
      },
      actions: [ new pac.actions.Commander() ],
      animations: new pac.AnimationList({
        off: new pac.Animation({
          fps: 1,
          frames: ['screen_center_off']
        }),
        matrix: new pac.Animation({
          fps: 4,
          frames: ['screen_center_0','screen_center_1']
        })
      }, {
        default: 'off',
        autoplay: false
      })
    });

    var serverDoorAnim = new pac.AnimationList({
      open: new pac.Animation({
        fps: 6,
        times: 1,
        frames: ['server_door_open','server_door_open_open']
      }),
      close: new pac.Animation({
        fps: 6,
        times: 1,
        frames: ['server_door_open_open', 'server_door_open','server_door_closed']
      }),
      closed: new pac.Animation({
        fps: 6,
        frames: ['server_door_closed']
      }),
    });

    var ServerDoor = ServersSprite.extend({
      name: 'Server Door',
      position: {
        x:140,
        y:40
      },
      size: {
        width: 37,
        height: 86
      },
      animations: serverDoorAnim,
      actions: [ new pac.actions.Commander() ],

      init: function(){
        this.animations.play('closed');
      },

    });

    var serverDoor = new ServerDoor();

    serverDoor.isClosed = true;
    serverDoor.onCommand = {
      open: function(){
        if (this.isClosed){
          this.animations.play('open');
          this.isClosed = false;
          return;
        }

        return 'That door is already open';
      },
      close: function(){
        if (this.isClosed){
          return 'That door is already closed';
        }

        this.animations.play('close');
        this.isClosed = true;
      }
    };

    var boss = new Boss({
      position: {
        x: 19,
        y: 19
      },
      actions: [
        new pac.actions.Tween({
          field: 'position',
          duration: 3.5,
          to: { y: 59 },
          easing: 'Elastic.InOut',
          repeat: true,
          yoyo: true
        }),
        new pac.actions.Tween({
          field: 'position',
          duration: 3,
          to: { x: 100 },
          easing: 'Elastic.InOut',
          repeat: true,
          yoyo: true
        })
      ]
    });

    this
    .addObject(servers)
    .addObject(rackOn)
    .addObject(rackError)
    .addObject(screen)
    .addObject(serverDoor)
    .addObject(boss)
    .addObject(door);

  },

  onExit: function(scene){
    ServersScene.__super__.onExit.apply(this, arguments);
  },

  update: function(dt){

  }
});

module.exports = ServersScene;
