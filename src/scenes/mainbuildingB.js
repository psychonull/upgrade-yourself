var pac = require('../../../pac/src/');
var _ = pac._;
var WalkableScene = require('../prefabs/WalkableScene.js');
var NPC1 = require('../prefabs/NPC1.js');
var Dialoguer = require('../actions/Dialoguer.js');
var Speaker = require('../actions/Speaker.js');

var MainBuildingBScene = WalkableScene.extend({

  size: { width: 320, height: 200 },
  texture: 'mainbuilding_B_bg',
  walkableShape: new pac.Polygon(
    [0,118,296,118,316,149,0,149]
  ),

  onEnter: function(scene){
    MainBuildingBScene.__super__.onEnter.apply(this, arguments);

    if (scene){
      switch(scene.name){
        case 'mainbuildingA':
          this.game.player.position = new pac.Point(50, 70);
          break;
        case 'travelfast':
          this.game.state.locations = [];
          this.game.player.position = new pac.Point(260, 60);
          break;
      }
    }
    else {
      this.game.player.position = new pac.Point(260, 60);
    }

    var inventory = this.game.findOne('Inventory');

    var MainBuildingSprite = pac.Sprite.extend({
      texture: 'mainbuilding_props',
      layer: 'front',
      shape: true,
      position: {
        x: 280,
        y: 20
      },
      size: {
        width: 32,
        height: 32
      }
    });

    var travelfast = new MainBuildingSprite({
      frame: 'travelfastpro',
      name: 'TravelFast Pro Limited Edition',
      layer: 'front',
      position: {
        x:253,
        y:44
      },
      size: {
        width: 48,
        height: 79
      },
      actions: [ new pac.actions.Commander(), new Speaker({ tint: 0xFFAAFF }) ]
    });

    travelfast.actions.pushFront(new Dialoguer({
      characters: {
        t: travelfast
      },
      dialogue: [{owner: 't', value: '42'}],
      command: 'talkto'
    })
    );

    travelfast.onCommand = {
      lookat: function(){
        return _.sample([
          'uhhmmm.. The PRO version?? Does this talk?'
        ]);
      },
      use: function(withName){
        if(!withName){
          this.game.loadScene('travelfast');
          return;
        }
        switch (withName){
          case 'home decoder':
            inventory.remove('home decoder');
            this.game.state.locations.push('room2');
            return false;
        }

        return 'I don\'t know how to use it with ' + withName;
      }
    };

    var desk = new MainBuildingSprite({
      frame: 'desk',
      name: 'Awesomely minimalistic desktop',
      layer: 'front',
      shape: new pac.Polygon(
        [0,0,19,0,29,10,65,10,77,0,97,1,104,10,104,51,6,51,0,40]),
      position: {
        x:90,
        y:80
      },
      size: {
        width: 105,
        height: 52
      },
      actions: [ new pac.actions.Commander() ]
    });

    desk.onCommand = {
      lookat: function(){
        return 'Nice stuff.';
      }
    };

    var npc = new NPC1({
      position: new pac.Point(120, 50),
      shape: new pac.Rectangle({
        size: {width: 32, height: 37}
      })
    });

    npc.actions.pushFront(new Speaker({
      tint: 0x00FF00
    }));

    npc.actions.pushFront(new Dialoguer({
      characters: {
        n: npc,
        p: this.game.findOne('Myself')
      },
      dialogue: require('../dialogues/mainbuildingB/npc1.js'),
      command: 'talkto'
    }));

    var changeScene = new pac.GameObject({
      layer: 'front',
      position: new pac.Point(0, 110),
      shape: new pac.Rectangle({ size: { width: 50, height: 40 }}),

      actions: [ new pac.actions.Commander() ]
    });

    changeScene.hiddenCommands = true;
    changeScene.onCommand = {
      walkto: function(){
        this.game.loadScene('mainbuildingA');
      }
    };

    this
      .addObject(changeScene)
      .addObject(travelfast)
      .addObject(npc)
      .addObject(desk);
  },

  onExit: function(scene){
    MainBuildingBScene.__super__.onExit.apply(this, arguments);
  },

  update: function(dt){

  }
});

module.exports = MainBuildingBScene;
