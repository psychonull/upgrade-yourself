var pac = require('../../../pac/src/');
var _ = pac._;
var WalkableScene = require('../prefabs/WalkableScene.js');
var Speaker = require('../actions/Speaker.js');
var Dialoguer = require('../actions/Dialoguer.js');

var RoomScene = WalkableScene.extend({

  size: { width: 320, height: 200 },
  texture: 'room2_bg',
  walkableShape: new pac.Polygon(
    [ 0,140 , 25,127 , 307,126 , 320,132, 320,150, 0,150 ]
  ),

  onEnter: function(scene){
    RoomScene.__super__.onEnter.apply(this, arguments);

    if(!this.game.state.room2){
      this.game.state.room2 = {
        firstTime: true,
        updaterThrown: false,
        updaterPickup: false
      };
    }
    this.game.state.locations = [];

    var inventory = this.game.findOne('Inventory');

    var RoomSprite = pac.Sprite.extend({
      texture: 'room_props',
      layer: 'background',
      shape: true,
      position: {
        x: 280,
        y: 20
      },
      size: {
        width: 32,
        height: 32
      }
    });

    var tube = new RoomSprite({
      frame: 'tube',
      name: 'Object transport device',
      position: {
        x:11,
        y:0
      },
      size: {
        width: 26,
        height: 116
      },
      actions: [ new pac.actions.Commander() ]
    });

    tube.onCommand = {
      use: function(){
        return _.sample([
          'I cannot make anything with it',
          'I guess I will need to wait for something to come..',
        ]);
      },
      pickup: function(){
        return _.sample([
          'I could pick it up if I were better designed..'
        ]);
      },
      lookat: function(){
        return _.sample([
          'The artifacts transport. Hope to receive a little gift soon',
          'It\'s old, but worked 81 years ago when I received my last package'
        ]);
      }
    };

    var travelfast = new RoomSprite({
      frame: 'travelfast',
      name: 'Travelfast Machine Lite Edition',
      position: {
        x:163,
        y:65
      },
      size: {
        width:43,
        height:69
      },
      actions: [
        new pac.actions.Commander(),
        new Speaker({ tint: 0xFFAAFF })
      ]
    });

    travelfast.onCommand = {
      lookat: function(){
        return _.sample([
          'Oldie but still rocks. Only if I had any location unlocked.',
          'The travelfast Lite edition was originally designed three centuries'+
            ' ago, but massive production kicked off 72 years ago.'
        ]);
      },
      use: function(withName){
        if(!withName){
          this.game.loadScene('travelfast');
          return;
        }
        switch (withName){
          case 'location decoder':
            inventory.remove('location decoder');
            this.game.state.locations.push('mainbuildingB');
            return false;
        }

        return 'I don\'t know how to use it with ' + withName;
      }
    };

    travelfast.actions.pushFront(new Dialoguer({
      characters: {
        t: travelfast,
        player: this.game.findOne('Myself')
      },
      dialogue: require('../dialogues/room/travelfast.js'),
      command: 'talkto'
    }));

    var charger = new RoomSprite({
      frame: 'charger',
      name: 'Charge station',
      position: {
        x:264,
        y:121
      },
      size: {
        width:33,
        height:19
      },
      actions: [ new pac.actions.Commander() ]
    });

    charger.onCommand = {
        lookat: function(){
          return _.sample([
            'The rest-bed-charge-station. ' +
              'State of the art biological and mechanical energy restore dock.'
            ]);
        },
        use: function(){
          return 'I don\'t wanna rest now';
        }
      };

    var chargerCables = new RoomSprite({
      frame: 'charger_cables',
      name: 'Charge station cables',
      position: {
        x:295,
        y:128
      },
      size: {
        width:25,
        height:22
      },
      actions: [ new pac.actions.Commander() ]
    });

    chargerCables.onCommand = {
      pickup: function(){
        if (inventory.has('Charge station cables')){
          return 'I already have it!';
        }

        var chargerCablesInv = new RoomSprite({
          frame: 'charger_cables',
          name: 'Charge station cables',
          actions: [ new pac.actions.Commander() ]
        });

        chargerCablesInv.onCommand = {
          use: function(withName){

            if (!withName){
              return 'I should use this with something';
            }

            switch (withName){
              case 'Travelfast Machine Lite Edition':
                inventory.remove(this.name);
                return false;
            }

            return 'I don\'t know how to use it with ' + withName;
          },
        };

        inventory.add(chargerCablesInv);
        return false;
      }
    };

    var viewpointAnimations = new pac.AnimationList({
      clouds: new pac.Animation({
        fps: 1,
        frames: ['viewpoint','viewpoint_1','viewpoint_2','viewpoint_3']
      }),
      commander: new pac.Animation({
        fps:2,
        frames: [
          'viewpoint_comm_0', 'viewpoint_comm_1', 'viewpoint_comm_2',
          'viewpoint_comm_2', 'viewpoint_comm_2', 'viewpoint_comm_1',
          'viewpoint_comm_0', 'viewpoint_comm_0', 'viewpoint_comm_2',
          'viewpoint_comm_1', 'viewpoint_comm_2', 'viewpoint_comm_0'
        ]
      })
    }, {
      default: 'commander',
      autoplay: true
    });

    var viewpoint = new RoomSprite({
      frame: 'viewpoint',
      name: 'Outside view',
      shape: new pac.Rectangle({
        size: { width: 63, height: 55 }
      }),
      animations: viewpointAnimations,
      position: {
        x:39,
        y:39
      },
      size: {
        width:63,
        height:60
      },
      actions: [
        new pac.actions.Commander() ,
        new Speaker({
          tint: 0xFF5555,
          font: '12px lucas'
        })
      ]
    });

    viewpoint.actions.pushFront(
      new Dialoguer({
        characters: {
          p: this.game.player,
          c: viewpoint
        },
        dialogue: require('../dialogues/room2/commander.js'),
        command: 'commanderunblock'
      })
    );

    var viewpointController = new RoomSprite({
      frame: 'viewpoint_controller_off',
      name: 'Outside view control panel',
      shape: new pac.Rectangle({
        position: new pac.Point(-5, 0),
        size: { width: 40, height: 6 }
      }),
      position: {
        x:60,
        y:95
      },
      size: {
        width:30,
        height:4
      },
      actions: [ new pac.actions.Commander() ]
    });

    viewpointController.onCommand = {
      push: function(){
        return 'Imscared';
      }
    };

    var updater = new pac.Sprite({
      texture: 'updater',
      layer: 'background',
      name: 'firmware update',
      shape: true,
      position: {
        x: 20,
        y: 128
      },
      size: {
        width: 12,
        height: 12
      },
      visible: false,
      actions: [ new pac.actions.Commander() ]
    });
    updater.active = false;

    updater.onCommand = {
      pickup: function(){
        this.visible = false;
        this.active = false;
        this.game.state.room2.updaterPickup = true;

        var updaterInv = new pac.Sprite({
          texture: 'updater',
          name: 'firmware update',
          actions: [ new pac.actions.Commander() ]
        });

        updaterInv.onCommand = {
          use: function(withName){

            if (!withName){
              return 'I should use this with something';
            }

            switch (withName){
              case 'Myself':
                inventory.remove(this.name);
                return false;
              }

              return 'I don\'t know how to use it with ' + withName;
            }
          };

          inventory.add(updaterInv);
          return false;
        }
      };

    if(this.game.state.room2.updaterThrown &&
      !this.game.state.room2.updaterPickup){
        updater.visible = true;
        updater.active = true;
      }

    this.game.player.actions.pushFront(new pac.actions.Commander());

    this.game.player.onCommand = {
      use: function(withName){
        if(withName === 'firmware update'){
          this.game.loadScene('end');
        }
      }
    };

    this.addObject(tube)
      .addObject(travelfast)
      .addObject(charger)
      .addObject(updater)
      .addObject(viewpoint)
      .addObject(viewpointController)
      .addObject(chargerCables);

    this.game.player.position = new pac.Point(172, 80);

    if(this.game.state.room2.firstTime){

      window.setTimeout(
        _.bind(function(){
          this.game.player.actions.pushFront(new pac.actions.WalkTo({
            target: new pac.Point(103, 75),
            velocity: 20
          }));
          viewpoint.onCommand.commanderunblock();
          viewpoint.dialogue.on('end', _.bind(function(){
            this.game.state.room2.firstTime = false;
            updater.visible = true;
            updater.active = true;
            this.game.state.room2.updaterThrown = true;
          },this));
        }, this), 20);

    }

  },

  onExit: function(scene){
    RoomScene.__super__.onExit.apply(this, arguments);
  },

  update: function(dt){

  }
});

module.exports = RoomScene;
