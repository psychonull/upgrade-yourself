var pac = require('../../../pac/src/');
var FirstPersonScene = require('../prefabs/FirstPersonScene.js');
var _ = pac._;

var MenuScene = FirstPersonScene.extend({

  size: { width: 320, height: 200 },

  onEnter: function(scene){
    MenuScene.__super__.onEnter.apply(this, arguments);
    /*var playLucas = new pac.Text('hello this is a test!', {
      font: '4px lucas',
      position: {
        x: 10,
        y: 20
      },
      isBitmapText: true,
      tint: 0x5500AA
    });
    var playLucaswhite = new pac.Text('Play!?', {
      font: '8px lucaswhite',
      position: {
        x: 10,
        y: 40
      },
      isBitmapText: true
    });
    var playLucasgray = new pac.Text('Play!?¿@', {
      font: '8px lucasgray',
      position: {
        x: 10,
        y: 60
      },
      isBitmapText: true
    });
    var playFunkygray = new pac.Text('Play the game!', {
      font: '16px funkygray',
      position: {
        x: 10,
        y: 80
      },
      isBitmapText: true
    });
    var playFunkywhite = new pac.Text('Play the goddamn game', {
      font: '8px funkywhite',
      position: {
        x: 10,
        y: 120
      },
      isBitmapText: true
    });

    this.addObject(playLucas)
      .addObject(playLucaswhite)
      .addObject(playLucasgray)
      .addObject(playFunkygray)
      .addObject(playFunkywhite);
      */
      this.startBtn = new pac.Text({
        value: 'I am ready.',
        font: '32px funkywhite',
        isBitmapText: true,
        position: {
          x:100,
          y:90
        },
        tint: 0xFF5555,
        actions: [new pac.actions.Hoverable(), new pac.actions.Clickable()],
        shape: new pac.Rectangle({ size: {
          width:120,
          height: 40
        } })
      });


      this.startBtn
      .on('hover:in', function(){
        this.tint = 0x00FF00;
      })
      .on('hover:out', function(){
        this.tint = 0xFF5555;
      })
      .on('click', function(){
        this.game.loadScene('room');
      });

      this.creditsBtn = new pac.Text({
        value: 'credits',
        font: '12px funkywhite',
        isBitmapText: true,
        position: {
          x:140,
          y:130
        },
        tint: 0xFF5555,
        actions: [new pac.actions.Hoverable(), new pac.actions.Clickable()],
        shape: new pac.Rectangle({ size: {
          width:70,
          height: 25
        } })
      });


      this.creditsBtn
      .on('hover:in', function(){
        this.tint = 0x00FF00;
      })
      .on('hover:out', function(){
        this.tint = 0xFF5555;
      })
      .on('click', _.bind(this.showCredits, this));

      this.backBtn = new pac.Text({
        visible: false,
        active: false,
        value: 'back',
        font: '16px funkywhite',
        isBitmapText: true,
        position: {
          x:200,
          y:150
        },
        tint: 0xFF5555,
        actions: [new pac.actions.Hoverable(), new pac.actions.Clickable()],
        shape: new pac.Rectangle({ size: {
          width:120,
          height: 40
        } })
      });

      this.backBtn
      .on('hover:in', function(){
        this.tint = 0x00FF00;
      })
      .on('hover:out', function(){
        this.tint = 0xFF5555;
      })
      .on('click', _.bind(this.backFromCredits, this));

      this
      .addObject(this.creditsBtn)
      .addObject(this.backBtn)
      .addObject(this.startBtn);
  },

  onExit: function(scene){
    MenuScene.__super__.onExit.apply(this, arguments);
  },

  update: function(dt){

  },

  showCredits: function(){

    if(!this.credits){

      this.credits  = [
        new pac.Text({
          font: '12px lucas',
          isBitmapText: true,
          wordWrap: 200,
          layer: 'front',
          value: 'This little experiment was made for the #pointclickjam.',
          actions: [new pac.actions.Hoverable(), new pac.actions.Clickable()],
          shape: new pac.Rectangle({ size: {
            width:200,
            height: 20
          } }),
          tint: 0xFFFFFF
        }),
        new pac.Text({
          font: '12px lucas',
          isBitmapText: true,
          wordWrap: 200,
          layer: 'front',
          value: 'An engine built by us was used.',
          actions: [new pac.actions.Hoverable(), new pac.actions.Clickable()],
          shape: new pac.Rectangle({ size: {
            width:200,
            height: 20
          } }),
          tint: 0xFFFFFF
        })
      ];

      _.forEach(this.credits, function(credit, i){
        credit.position = {
          x: 40,
          y: 50*i
        };
        credit.on('hover:in', function(){
          this.tint = 0x00FF00;
        }).on('hover:out', function(){
          delete this.tint;
        });
        this.addObject(credit);
      }, this);
    }
    this.creditsBtn.visible = false;
    this.creditsBtn.active = false;
    this.startBtn.visible = false;
    this.startBtn.active = false;
    _.forEach(this.credits, function(c){
      c.visible = true;
      c.active = true;
    });
    this.backBtn.visible = true;
    this.backBtn.active = true;

  },

  backFromCredits: function(){
    this.creditsBtn.visible = true;
    this.creditsBtn.active = true;
    this.startBtn.visible = true;
    this.startBtn.active = true;
    _.forEach(this.credits, function(credit, i){
      credit.visible = false;
      credit.active = false;
    }, this);
    this.backBtn.visible = false;
    this.backBtn.active = false;
  }

});

module.exports = MenuScene;
