var pac = require('../../../pac/src/');
var FirstPersonScene = require('../prefabs/FirstPersonScene.js');
var _ = pac._;

var MenuScene = FirstPersonScene.extend({

  size: { width: 320, height: 200 },

  onEnter: function(scene){
    MenuScene.__super__.onEnter.apply(this, arguments);
    /*var playLucas = new pac.Text('hello this is a test!', {
    font: '4px lucas',
    position: {
    x: 10,
    y: 20
  },
  isBitmapText: true,
  tint: 0x5500AA
});
var playLucaswhite = new pac.Text('Play!?', {
font: '8px lucaswhite',
position: {
x: 10,
y: 40
},
isBitmapText: true
});
var playLucasgray = new pac.Text('Play!?¿@', {
font: '8px lucasgray',
position: {
x: 10,
y: 60
},
isBitmapText: true
});
var playFunkygray = new pac.Text('Play the game!', {
font: '16px funkygray',
position: {
x: 10,
y: 80
},
isBitmapText: true
});
var playFunkywhite = new pac.Text('Play the goddamn game', {
font: '8px funkywhite',
position: {
x: 10,
y: 120
},
isBitmapText: true
});

this.addObject(playLucas)
.addObject(playLucaswhite)
.addObject(playLucasgray)
.addObject(playFunkygray)
.addObject(playFunkywhite);
*/
var playLucaswhite = new pac.Text('The End.', {
  font: '8px lucaswhite',
  position: {
    x: 10,
    y: 40
  },
  isBitmapText: true
});

this.addObject(playLucaswhite);
},

onExit: function(scene){
  MenuScene.__super__.onExit.apply(this, arguments);
},

update: function(dt){

}

});

module.exports = MenuScene;
