var pac = require('../../../pac/src/');

var Speak = pac.actions.Speak.extend({

  durationOffsetFn: function(textValue){
    return textValue.length * 0.05;
  },

  wordWrapCorrectionFn: function(text){
    if(text.value.length < 50){
      text.wordWrap = 120;
    }
    else if(text.value.length < 100 ){
      text.wordWrap = 170;
    }
    else{
      text.wordWrap = 220;
    }
  }

});

module.exports = Speak;
