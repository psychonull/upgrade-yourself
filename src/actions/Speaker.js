var pac = require('../../../pac/src/');

var Speaker = pac.actions.Speaker.extend({
  textOptions: {
    wordWrap: 125,
    font: '6px lucas',
    isBitmapText: true,
    layer: 'front',
    zIndex: 100,
    tint: 0xFFFFFF,
    wrapToScreen: true
  },
  offset: new pac.Point(-20,-10),
  smartPosition: false,

  init: function(options){
    if (options && options.tint){
      this.tint = options.tint;
    }
    else {
      this.tint = 0xFFFFFF;
    }
    if (options && options.font){
      this.font = options.font;
    }
    if(options && options.avoidAutoWrap){
      this.avoidAutoWrap = true;
    }
    this.constructor.__super__.init.call(this, options);
  },

  onStart: function(){
    this.constructor.__super__.onStart.call(this);
    this.actions.owner.speakerText.tint = this.tint;
    if(this.font){
      this.actions.owner.speakerText.font = this.font;
    }
    if(this.avoidAutoWrap){
      this.actions.owner.speakerText.avoidAutoWrap = true;
    }
  }
});

module.exports = Speaker;
