var pac = require('../../../pac/src/');

var Dialoguer = pac.actions.Dialoguer.extend({

  init: function(options){
    this.constructor.__super__.init.call(this, options);
    this.options.speakClass = require('./Speak');
  },

  onDialogueCommand: function(){
    this.actions.owner.game.cinematic(true);
    this.constructor.__super__.onDialogueCommand.call(this);
  },

  onDialogueEnd: function(){
    this.actions.owner.game.cinematic(false);
    this.constructor.__super__.onDialogueEnd.call(this);
  }
});

module.exports = Dialoguer;
