var pac = require('../../pac/src/'),
  createGame = require('./createGame.js'),
  Player = require('./prefabs/Player.js'),
  Loading = require('./Loading.js');

window.document.title += ' [pac  v' + pac.VERSION + ']';
pac.DEBUG = false;

var ctn = window.document.getElementById('content');

var game = pac.create();
var gameScale = ctn.clientHeight / 200;

game.use('loader', pac.Loader, require('./assets.js'));

game.use('renderer', pac.PixiRenderer, {
  container: ctn,
  backgroundColor: '#000000',
  size: {
    width: 320,
    height: 200
  },
  layers: ['background', 'front', 'dialogue', 'gui'],
  scale: gameScale
});

game.use('input', [pac.MouseInput, pac.KeyboardInput], {
  enabled: true,
  scale: gameScale,
  keys: ['A', 'S', 'D', 'W']
});

game.use('scenes', require('./scenes'));

game.loader.on('progress', Loading.update);

game.loader.on('complete', function(){
  Loading.update(1);
  Loading.ready(3500, function(){
    game.start('room');
  });
});

game.on('ready', createGameObjects);

if (pac.DEBUG){
  window.addEventListener('load', function(){
    require('./editor')(game);
  });
}

Loading.create();
game.loader.load();

game.cinematic = function(bool){
  this.commandBar.visible = !bool;
  this.commandBar.active = !bool;
  this.inventory.visible = !bool;
  this.inventory.active = !bool;
  this.isCinematic = bool;
};

// hack for develop
window.game = game;

function createGameObjects(){
  this.player = new Player({});
  window.player = this.player;

  this.inventory = require('./gui/inventory.js');
  this.commandBar = require('./gui/commandBar.js');
  this.dialogueOptionsBar = require('./gui/dialogueOptionsBar.js');

  this.commandBar.inventory = this.inventory;

  this
    .addObject(this.player)
    .addObject(this.inventory)
    .addObject(this.commandBar)
    .addObject(this.dialogueOptionsBar);

  this.state = {
    locations: []
  };
}
