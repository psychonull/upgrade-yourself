var _ = require('../../pac/src/pac')._;

var fonts = {
  'lucas': {
    texture: 'assets/fonts/lucas/lucas.png',
    definition: 'assets/fonts/lucas/font.json',
    type: 'bitmapFont'
  },
  'lucasgray': {
    texture: 'assets/fonts/lucasgray/lucasgray.png',
    definition: 'assets/fonts/lucasgray/font.json',
    type: 'bitmapFont'
  },
  'lucaswhite': {
    texture: 'assets/fonts/lucaswhite/lucaswhite.png',
    definition: 'assets/fonts/lucaswhite/font.json',
    type: 'bitmapFont'
  },
  'funkygray': {
    texture: 'assets/fonts/funkygray/font.png',
    definition: 'assets/fonts/funkygray/font.json',
    type: 'bitmapFont'
  },
  'funkywhite': {
    texture: 'assets/fonts/funkywhite/font.png',
    definition: 'assets/fonts/funkywhite/font.json',
    type: 'bitmapFont'
  }
};

var textures = {
  'player': {
    path: 'assets/img/cyborg.png',
    atlas: 'assets/img/cyborg.json'
  },
  'room_bg': 'assets/img/room/Room_with_shadows.png',
  'room_props': {
    path: 'assets/img/room/room_props.png',
    atlas: 'assets/img/room/room_props.json'
  },

  'travelfast_bg': 'assets/img/travelfast/background.png',
  'travelfast_machine_full': 'assets/img/travelfast/Screen.png',

  'mainbuilding_props': {
    path: 'assets/img/mainbuilding/mainbuilding.png',
    atlas: 'assets/img/mainbuilding/mainbuilding.json'
  },
  'mainbuilding_A_bg': 'assets/img/mainbuilding/background_left.png',
  'mainbuilding_B_bg': 'assets/img/mainbuilding/background_right.png',
  'npc1': {
    path: 'assets/img/npc1.png',
    atlas: 'assets/img/npc1.json'
  },

  'servers_bg': 'assets/img/servers/background.png',
  'servers_props': {
    path: 'assets/img/servers/servers_props.png',
    atlas: 'assets/img/servers/servers_props.json'
  },
  'boss': {
    path: 'assets/img/boss.png',
    atlas: 'assets/img/boss.json'
  },

  'location_decoder': 'assets/img/items/props/location_decoder.png',

  'room2_bg': 'assets/img/room2/Room_with_shadows.png',
  'updater': 'assets/img/items/props/updater.png'
};

module.exports = _.merge(fonts, textures);
